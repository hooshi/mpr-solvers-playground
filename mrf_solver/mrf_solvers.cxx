//
// Some tools for wrapping mrf solvers.
//
// Shayan Hoshyari Jun 2018
//

#include <bits/unique_ptr.h>
#include <cstdlib>

#include "mrf_solver/mrf_solvers.hxx"

// #define POLYCUT_HAS_GCO
#if !defined(POLYCUT_HAS_MAPMAP)
#define POLYCUT_HAS_MAPMAP
#endif

#if defined(POLYCUT_HAS_GCO)
#define GCO_ENERGY_TYPE dgp::mrf::Scalar_type
#define GCO_ENERGY_TERM_TYPE dgp::mrf::Scalar_type
#define GCO_PROVIDE_IMPLEMENTATION
#include "gco/GCoptimization.h"
#endif
#if defined(POLYCUT_HAS_MAPMAP)
#include "mapmap/full.h"
#endif

namespace dgp
{
namespace mrf
{

// ==============================================================================
//                                FACTORY
// ==============================================================================

MRF_solver_base *
build_mrf_solver(const MRF_solver_type type, const Index_type n_vertices, const Index_type n_labels)
{
  switch(type)
    {
      case MRF_SOLVER_GCO: return new MRF_solver_gco(n_vertices, n_labels);
      case MRF_SOLVER_MAPMAP: return new MRF_solver_mapmap(n_vertices, n_labels);
      default: return (MRF_solver_base *)NULL;
    }
}

// ==============================================================================
//                                BASE
// ==============================================================================

MRF_solver_base::MRF_solver_base(const Index_type n_vertices_in, const Index_type n_labels_in)
  : _n_vertices(n_vertices_in), _n_labels(n_labels_in), _is_graph_finalized(false), _is_cost_finalized(false)
{
}

void
MRF_solver_base::finalize_graph()
{
  _is_graph_finalized = true;
}

void
MRF_solver_base::finalize_cost()
{
  _is_cost_finalized = true;
}

// ==============================================================================
//                                GCO
// ==============================================================================
#if defined(POLYCUT_HAS_GCO)

struct MRF_solver_gco::Internal_data
{
  std::unique_ptr<GCoptimizationGeneralGraph> gc;
};

MRF_solver_gco::MRF_solver_gco(const Index_type n_vertices_in, const Index_type n_labels_in)
  : MRF_solver_base(n_vertices_in, n_labels_in)
{
  _data = new MRF_solver_gco::Internal_data;
  data()->gc.reset(new GCoptimizationGeneralGraph(n_vertices(), n_labels()));
}

// For now I will just add the interface that has been used in polycut
void
MRF_solver_gco::add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight)
{
  data()->gc->setNeighbors(vertex0, vertex1, weight);
}

void
MRF_solver_gco::set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids,
                                const std::vector<Scalar_type> &unary_terms)
{
  assert(unary_terms.size() == vertex_ids.size());
  std::vector<GCoptimizationGeneralGraph::SparseDataCost> pairs(unary_terms.size());
  for(unsigned ii = 0; ii < unary_terms.size(); ++ii)
    {
      pairs[ii].cost = unary_terms[ii];
      pairs[ii].site = vertex_ids[ii];
    }
  data()->gc->setDataCost(label_id, pairs.data(), (int)pairs.size());
}

void
MRF_solver_gco::set_binary_costs(Pairwise_cost_function func, void *ctx)
{
  data()->gc->setSmoothCost(func, ctx);
}

void
MRF_solver_gco::solve(std::vector<Index_type> &labels, Scalar_type &energy)
{
  assert(_is_graph_finalized);
  assert(_is_cost_finalized);

  energy = data()->gc->swap(-1);
  labels.resize(n_vertices());
  for(Index_type ii = 0; ii < n_vertices(); ++ii)
    {
      labels[ii] = data()->gc->whatLabel(ii);
    }
}

#else
// clang-format off
struct MRF_solver_gco::Internal_data{};
MRF_solver_gco::MRF_solver_gco(const Index_type n_vertices_in, const Index_type n_labels_in):MRF_solver_base(-1,-1){}
void MRF_solver_gco::add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight){}
void MRF_solver_gco::set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids, const std::vector<Scalar_type> &unary_terms){}
void MRF_solver_gco::set_binary_costs(Pairwise_cost_function, void *ctx){}
void MRF_solver_gco::solve(std::vector<Index_type> &labels, Scalar_type &energy){}
// clang-format on
#endif

// ==============================================================================
//                                MAPMAP
// ==============================================================================
#if defined(POLYCUT_HAS_MAPMAP)
struct MRF_solver_mapmap::Internal_data
{
  static constexpr mapmap::uint_t simd_w = mapmap::sys_max_simd_width<Scalar_type>();
  typedef mapmap::UnaryTable<Scalar_type, simd_w> Unary_cost_type;
  typedef mapmap::PairwiseTable<Scalar_type, simd_w> Pairwise_cost_type;


  // Unlike GCO, I have to cache these data until finalize is called
  // clang-format off
  std::vector<Index_type> cached_edges;
  Index_type n_edges() { return cached_edges.size() / 2; }
  std::vector<Scalar_type> cached_unary_costs;
  Pairwise_cost_function cached_pairwise_cost_function;
  void *cached_pairwise_cost_ctx;
  // clang-format on

  // members
  std::unique_ptr<mapmap::Graph<Scalar_type>> graph;
  std::unique_ptr<mapmap::LabelSet<Scalar_type, simd_w>> label_set;
  std::vector<std::unique_ptr<Unary_cost_type>> unariy_costs;
  std::vector<std::unique_ptr<Pairwise_cost_type>> pairwise_costs;

  // termination criterion
  // std::unique_ptr<mapmap::TerminationCriterion<Scalar_type, simd_w>> terminate;

  // solver options
  // mapmap::mapMAP_control ctr;

  // solver
  // mapmap::mapMAP<Scalar_type, simd_w> mapmap_solver;

  // To remove
  // std::unique_ptr<GCoptimizationGeneralGraph> gc;
};

MRF_solver_mapmap::MRF_solver_mapmap(const Index_type n_vertices_in, const Index_type n_labels_in)
  : MRF_solver_base(n_vertices_in, n_labels_in)
{
  _data = new MRF_solver_mapmap::Internal_data;
  data()->graph.reset(new mapmap::Graph<Scalar_type>(n_vertices()));
  data()->cached_unary_costs.resize(n_vertices() * n_labels(), 0);
  data()->cached_pairwise_cost_function = NULL;
  data()->cached_pairwise_cost_ctx = NULL;
}

void
MRF_solver_mapmap::add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight)
{
  // Cache for applying costs.
  data()->graph->add_edge(vertex0, vertex1, weight);
  data()->cached_edges.push_back(vertex0);
  data()->cached_edges.push_back(vertex1);
}

void
MRF_solver_mapmap::set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids,
                                   const std::vector<Scalar_type> &unary_terms)
{
  // Cache, apply later.
  assert(unary_terms.size() == vertex_ids.size());
  for(unsigned ii = 0; ii < unary_terms.size(); ++ii)
    {
      const Index_type vid = vertex_ids[ii];
      const Scalar_type cost = unary_terms[ii];
      data()->cached_unary_costs[vid * n_labels() + label_id] = cost;
    }
}

void
MRF_solver_mapmap::set_binary_costs(Pairwise_cost_function func, void *ctx)
{
  data()->cached_pairwise_cost_function = func;
  data()->cached_pairwise_cost_ctx = ctx;
}

// call this before calling solve
void
MRF_solver_mapmap::finalize_graph()
{
  data()->graph->update_components();
  MRF_solver_base::finalize_graph();
}

void
MRF_solver_mapmap::finalize_cost()
{
  assert(_is_graph_finalized);

  // alias
  constexpr Index_type simd_w = MRF_solver_mapmap::Internal_data::simd_w;
  typedef MRF_solver_mapmap::Internal_data::Unary_cost_type Unary_cost_type;
  typedef MRF_solver_mapmap::Internal_data::Pairwise_cost_type Pairwise_cost_type;

  //
  // Let's build ...
  //

  //
  // Create the label set
  // false is for no compression
  //
  data()->label_set.reset(new mapmap::LabelSet<Scalar_type, simd_w>(n_vertices(), false));
  {
    std::vector<mapmap::_iv_st<Scalar_type, simd_w>> fixed_labels(n_labels());
    for(Index_type ll = 0; ll < n_labels(); ++ll)
      {
        fixed_labels[ll] = ll;
      }
    for(Index_type vid = 0; vid < n_vertices(); ++vid)
      {
        data()->label_set->set_label_set_for_node(vid, fixed_labels);
      }
  }

  //
  // Unary costs for each label
  //
  data()->unariy_costs.resize(n_vertices());
  for(Index_type vid = 0; vid < n_vertices(); ++vid)
    {
      std::vector<mapmap::_s_t<Scalar_type, simd_w>> costs(n_labels());
      for(Index_type ll = 0; ll < n_labels(); ll++)
        {
          costs[ll] = data()->cached_unary_costs[vid * n_labels() + ll];
        }

      data()->unariy_costs[vid].reset(new Unary_cost_type(vid, data()->label_set.get()));
      data()->unariy_costs[vid]->set_costs(costs);
    }

  //
  // Pairwise costs for each label
  //
  assert(data()->cached_pairwise_cost_function);
  data()->pairwise_costs.resize(data()->n_edges());
  for(Index_type eid = 0; eid < data()->n_edges(); ++eid)
    {
      std::vector<mapmap::_s_t<Scalar_type, simd_w>> costs(n_labels() * n_labels());
      const Index_type v1 = data()->cached_edges[eid * 2 + 0];
      const Index_type v2 = data()->cached_edges[eid * 2 + 1];
      void *ctx = data()->cached_pairwise_cost_ctx;

      for(Index_type l1 = 0; l1 < n_labels(); ++l1)
        {
          for(Index_type l2 = 0; l2 < n_labels(); ++l2)
            {
              costs[l1 * n_labels() + l2] = data()->cached_pairwise_cost_function(v1, v2, l1, l2, ctx);
            }
        }
      data()->pairwise_costs[eid].reset(new Pairwise_cost_type(v1, v2, data()->label_set.get(), costs));
    }

  MRF_solver_base::finalize_cost();
}


void
MRF_solver_mapmap::solve(std::vector<Index_type> &labels, Scalar_type &energy)
{
  assert(_is_cost_finalized);
  assert(_is_graph_finalized);

  // alias
  constexpr Index_type simd_w = MRF_solver_mapmap::Internal_data::simd_w;
  // typedef MRF_solver_mapmap::Internal_data::Unary_cost_type Unary_cost_type;
  // typedef MRF_solver_mapmap::Internal_data::Pairwise_cost_type Pairwise_cost_type;

  // termination criterion
  std::unique_ptr<mapmap::TerminationCriterion<Scalar_type, simd_w>> terminate;
  // solver options
  mapmap::mapMAP_control ctr;
  // solver
  mapmap::mapMAP<Scalar_type, simd_w> mapmap_solver;

  // SOme default termination criteria
  terminate.reset(new mapmap::StopWhenReturnsDiminish<Scalar_type, simd_w>(5, 0.0001));

  //
  // Set up the solver
  //
  mapmap_solver.set_graph(data()->graph.get());
  mapmap_solver.set_label_set(data()->label_set.get());
  for(Index_type n = 0; n < n_vertices(); ++n)
    {
      mapmap_solver.set_unary(n, data()->unariy_costs[n].get());
    }
  for(Index_type n = 0; n < data()->n_edges(); ++n)
    {
      mapmap_solver.set_pairwise(n, data()->pairwise_costs[n].get());
    }
  mapmap_solver.set_termination_criterion(terminate.get());

  //
  // Some default options
  //
  ctr.use_multilevel = true;
  ctr.use_spanning_tree = true;
  ctr.use_acyclic = true;
  ctr.spanning_tree_multilevel_after_n_iterations = 5;
  ctr.force_acyclic = true;
  ctr.min_acyclic_iterations = 5;
  ctr.relax_acyclic_maximal = true;
  ctr.tree_algorithm = mapmap::LOCK_FREE_TREE_SAMPLER;
  /* set to true and select a seed for (serial) deterministic sampling */
  ctr.sample_deterministic = false;
  ctr.initial_seed = 548923723;

  //
  // Run the solver
  //
  std::vector<mapmap::_iv_st<Scalar_type, simd_w>> solution;
  mapmap::_s_t<Scalar_type, simd_w> energy_fancy = mapmap_solver.optimize(solution, ctr);
  energy = energy_fancy;

  // Extract the solution
  labels.resize(n_vertices());
  for(Index_type n = 0; n < n_vertices(); ++n)
    {
      labels[n] = data()->label_set->label_from_offset(n, solution[n]);
      // printf("Node %d: l %d s %d \n", n, labeling[n], solution[n]);
    }
}

#else
// clang-format off
struct MRF_solver_mapmap::Internal_data{};
MRF_solver_mapmap::MRF_solver_mapmap(const Index_type n_vertices_in, const Index_type n_labels_in):MRF_solver_base(-1,-1){}
void MRF_solver_mapmap::add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight){}
void MRF_solver_mapmap::set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids, const std::vector<Scalar_type> &unary_terms){}
void MRF_solver_mapmap::set_binary_costs(Pairwise_cost_function, void *ctx){}
void MRF_solver_mapmap::finalize_graph(){}
void MRF_solver_mapmap::finalize_cost(){}
void MRF_solver_mapmap::solve(std::vector<Index_type> &labels, Scalar_type &energy){ throw "Need mapmap"; }
// clang-format on
#endif


} // namespace mrf
} // namespace dgp