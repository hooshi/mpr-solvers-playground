//
// Some tools for wrapping mrf solvers.
//
// Shayan Hoshyari Jun 2018
//

#include <vector>

#include <cassert>

namespace dgp
{
namespace mrf
{
// ==============================================================================
//                                  ENUM, TYPEDEF, ETC.
// ==============================================================================

typedef int Index_type;
typedef double Scalar_type;
typedef Scalar_type (*Pairwise_cost_function)(Index_type s1, Index_type s2, Index_type l1, Index_type l2, void *ctx);


enum MRF_solver_type
{
  MRF_SOLVER_GCO = 0,
  MRF_SOLVER_MAPMAP
};

// ==============================================================================
//                                  FACTORY
// ==============================================================================

class MRF_solver_base;
MRF_solver_base *build_mrf_solver(const MRF_solver_type type, const Index_type n_vertices, const Index_type n_labels);


// ==============================================================================
//                                  BASE
// ==============================================================================

class MRF_solver_base
{
public:
  MRF_solver_base(const Index_type n_vertices_in, const Index_type n_labels_in);

  // For now I will just add the interface that has been used in polycut
  virtual void add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight = 1) = 0;
  virtual void set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids,
                               const std::vector<Scalar_type> &unary_terms) = 0;
  virtual void set_binary_costs(Pairwise_cost_function, void *ctx) = 0;

  // call this before calling solve
  virtual void finalize_graph();
  virtual void finalize_cost();
  virtual void solve(std::vector<Index_type> &labels, Scalar_type &energy) = 0;

  // clang-format off
  Index_type n_vertices() { assert(_n_labels >= 0); return _n_vertices; }
  Index_type n_labels() { assert(_n_labels >= 0); return _n_labels; }
  // clang-format on


protected:
  Index_type _n_vertices;
  Index_type _n_labels;
  bool _is_graph_finalized;
  bool _is_cost_finalized;
};



// ==============================================================================
//                                GCO
// FOR NOW NO TBB
// ==============================================================================
class MRF_solver_gco : public MRF_solver_base
{
public:
  MRF_solver_gco(const Index_type n_vertices_in, const Index_type n_labels_in);

  // For now I will just add the interface that has been used in polycut
  virtual void add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight = 1);
  virtual void set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids,
                               const std::vector<Scalar_type> &unary_terms);
  virtual void set_binary_costs(Pairwise_cost_function, void *ctx);

  // call this before calling solve
  virtual void solve(std::vector<Index_type> &labels, Scalar_type &energy);


protected:
  struct Internal_data;
  Internal_data *_data;

  // clang-format off
  Internal_data* data(){ return _data; }
  // clang-format on
};

// ==============================================================================
//                                MAPMAP
// ==============================================================================
class MRF_solver_mapmap : public MRF_solver_base
{
public:
  MRF_solver_mapmap(const Index_type n_vertices_in, const Index_type n_labels_in);

  // For now I will just add the interface that has been used in polycut
  virtual void add_edge(const Index_type vertex0, const Index_type vertex1, const Scalar_type weight = 1);
  virtual void set_unary_costs(const Index_type label_id, const std::vector<Index_type> &vertex_ids,
                               const std::vector<Scalar_type> &unary_terms);
  virtual void set_binary_costs(Pairwise_cost_function, void *ctx);

  // call this before calling solve
  virtual void finalize_graph();
  virtual void finalize_cost();
  virtual void solve(std::vector<Index_type> &labels, Scalar_type &energy);

protected:
  struct Internal_data;
  Internal_data *_data;

  // clang-format off
  Internal_data* data(){ return _data; }
  // clang-format on
};

} // namespace mrf
} // namespace dgp