add_executable(mapmap_demo	mapmap_demo.cxx)
target_link_libraries(mapmap_demo ${TBB_LIBRARIES})

add_executable(gco_demo	gco_demo.cxx)
# target_link_libraries(gco_demo GCO_LIBRARY)

add_executable(gco_mapmap_comparison	gco_mapmap_comparison.cxx)
target_link_libraries(gco_mapmap_comparison MRF_SOLVER_LIBRARY ${TBB_LIBRARIES})

add_executable(test_mrf	test_mrf.cxx)
target_link_libraries(test_mrf MRF_SOLVER_LIBRARY ${TBB_LIBRARIES} )
